var path = require('path');
var express = require('express'),
    app = express(),
    http = require('http'),
    socketIo = require('socket.io')
// creat server
var server = http.createServer(app)
//listening
var io = socketIo.listen(server)
server.listen(8080)
// servering html
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/js/start.html'))
});
// dec. variable player2 to check when the player2 is connected
var player2 = false
app.use(express.static("."))
// user id to identify the connected users
let user_id = 0
console.log("Server running on http://127.0.0.1:8080")
// current users
var currentUsers = 0
/// max users are two
var maxUsers = 2
// dctionary {socketid:numbers}
var Player_Index_arr = {}
// connection
io.on('connection', function (socket) {
    // increasing the currentusers
    currentUsers++
    // if there are more than 2 users then disconnect
    if (currentUsers > maxUsers) {
        console.log("sorry too many connections, max. are 2 clients allowed")
        socket.disconnect()
        return
    } else {
        // increasing the userid
        user_id++
        // mapping socketid with numbers
        if (user_id === 1) {
            Player_Index_arr[socket.id] = 1
        }
        if (user_id === 2) {
            Player_Index_arr[socket.id] = 2
            // the userid 2 is connected then player2 is true
            player2 = true
        }
        console.log("Client (" + Player_Index_arr[socket.id] + ") is connected")
        // update data send the data to all the clint
        socket.on('updateData', function (data) {
            // send the data to all the client except the client who sent the change(Broadcasting)
            socket.broadcast.emit('updateData', data)
        });
        // added line
        socket.on('added', function (leng) {
            socket.broadcast.emit('added', leng)
        });
        // gameover
        socket.on('over', function (winner) {
            socket.broadcast.emit('over', winner)
        });
        // winner name
        socket.on('timeOutWinner', function (winner) {
            socket.broadcast.emit('timeOutWinner', winner)
        });
        // disconnect the users
        socket.on('disconnect', function () {
            console.log("Client (" + Player_Index_arr[socket.id] + ") is disconnected")
            user_id--
            currentUsers--
        })
    }
    // player2 ist online
    if (player2) {
        // player2 is online
        socket.on('online', function () {
            socket.broadcast.emit('online')
        })
        player2 = false
    }
});