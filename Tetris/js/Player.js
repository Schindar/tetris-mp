export default class Player {
    constructor(form, boardLeft, boardRight) {
        this.form = form
        this.boardLeft = boardLeft
        this.boardRight = boardRight
        this.scoreOfBoxLeft = 0
        this.scoreOfBoxRight = 0
        this.linesOfBoxLeft = 0
        this.linesOfBoxRight = 0
        this.status = false
        this.playerName = document.getElementById.innerHTML = localStorage.userName
        this.getTimer = document.getElementById.innerHTML = localStorage.setTimer
    }
    // moving
    move(event) {
        // LEFT
        if (event.keyCode === 65 && this.form.lock === false && this.formRight.lock === false) { //left
            this.form.rotdir = 0
            this.form.xp = this.form.x
            this.form.x -= 1 // active form is moving to the left
            this.PieceMoveSound.play() // RIGHT
        } else if (event.keyCode === 68 && this.form.lock === false && this.formRight.lock === false) { //right
            this.form.rotdir = 0
            this.form.xp = this.form.x
            this.form.x += 1 // active form is moving to the right
            this.PieceMoveSound.play() // RIGHT
            // ROTATE
        } else if (event.keyCode === 69 && this.form.lock === false && this.formRight.lock === false) { //rot CW
            this.form.rotdir = 1
            this.form.rotate(this.form.Array2d) // active form is rotating
            // kick to the leveeft or right if there is a collision
            // DOWN
            this.rotateSoundEffect.play()
        } else if (event.keyCode === 81 && this.form.lock === false && this.formRight.lock === false) { //rot CCW
            this.form.rotdir = -1
            this.form.rotatereverse(this.form.Array2d) // active form is rotating
            this.rotateSoundEffect.play()
        } else if (event.keyCode === 83 && this.form.lock === false && this.formRight.lock === false) { //down
            this.form.rotdir = 0
            this.form.xp = this.form.x
            this.form.yp = this.form.y
            this.form.y += 1 // active form is moving down
            // this.player1.scoreOfBoxLeft += 1
            // QUICKLY DOWN
            this.softDownSound.play()
        } else if (event.key == ' ' && this.form.lock === false && this.formRight.lock === false) { // spacebar button
            this.dropInterval = 1 // active form is moving down quickly
            // this.player1.scoreOfBoxLeft += this.height - this.form.x
            this.dropDownSoundEffect.play()
        }
    }
    //
    moveKeyUp(e) {
        this.dropInterval = 1000
    }
}