export default class Client {
    constructor(boardLeft, form, player1, player2, boxLeft, boxRight, boardRight) {
        this.boardLeft = boardLeft
        this.boxLeft = boxLeft
        this.boxRight = boxRight
        this.boardRight = boardRight
        this.form = form
        this.player1 = player1
        this.player2 = player2
    }
    // check when the player2 is online
    player2IsOnline(socket) {
        socket.emit('online');
        var player1 = this.player1
        var player2 = this.player2
        socket.on('online', function () {
            player1.status = true
            player2.status = true
        });
    }
    //updating the data
    updateData(form, boardLeft, player1, boardRight, player2, formRight, socket) {
        // object for x and y cor. of the form
        var informations = {
            x: form.x,
            y: form.y,
            array: form.Array2d,
            board: boardLeft.board,
            flagge: form.removedLine,
            lines: player1.linesOfBoxLeft,
            scores: player1.scoreOfBoxLeft,
            playerName: player1.playerName,
            lock: form.lock,
            timer: boardLeft.timer
        }
        // send the information to the server
        socket.emit('updateData', informations)
        var formRight = formRight
        var boardRight = boardRight
        var player2 = player2
        // update the information received from the server
        socket.on('updateData', function (data) {
            // clearing the canvas_right after update
            formRight.x = data.x
            formRight.y = data.y
            formRight.Array2d = data.array
            boardRight.board = data.board
            player2.linesOfBoxLeft = data.lines
            player2.scoreOfBoxLeft = data.scores
            player2.playerName = data.playerName
            formRight.lock = data.lock
            boardRight.timer = data.timer
        });
    }
    //  send message to the server that the line was removed
    addLinesServer(boardRight, boardLeft, socket) {
        var boardRight = boardRight
        var boardLeft = boardLeft
        //send the information to the server if the the line has to be added
        socket.emit('added', boardLeft.laenge)
        // excute the add lines functions
        socket.on('added', function (leng) {
            boardLeft.addLines(boardLeft.board, leng)
            boardRight.addLines(boardRight.board, leng)
        });
    }
    //game over if the form is up to the board
    over(socket, player1, player2, form, boardLeft) {
        var boardLeft = boardLeft
        var form = form
        var player1 = player1
        var player1 = player2
        socket.on('over', function (winner) {
            document.getElementById("spanWinner").innerHTML = winner.playerName;
            boardLeft.popup(player1, winner)
        });
    }
    timeOutWinnerServer(socket, player1, player2, boardLeft) {
        var boardLeft = boardLeft
        socket.on('timeOutWinner', function (winner) {
            console.log(player1.scoreOfBoxLeft + "score " + winner.scoreOfBoxLeft)
            console.log(player1.playerName + "name  " + winner.playerName)
            boardLeft.popup(player1, winner)
            // check  the wining case
            document.getElementById("spanWinner").innerHTML = winner.playerName;
            if (player1.scoreOfBoxLeft === winner.scoreOfBoxLeft) {
                document.getElementById("spanWinner").innerHTML = "draw, no winner"
                boardLeft.popup(player1, winner)
            } else if (player1.scoreOfBoxLeft > winner.scoreOfBoxLeft) {
                document.getElementById("spanWinner").innerHTML = player1.playerName
                boardLeft.popup(player1, winner)
            } else {
                console.log("______")
                document.getElementById("spanWinner").innerHTML = winner.playerName
                boardLeft.popup(player1, winner)
            }
        });
    }
}