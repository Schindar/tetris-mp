export default class Board {
    constructor(width, height, getTimer, form, assets) {
        this.form = form
        this.assets = assets
        this.board = this.createMatrix(width, height)
        this.rowsLength
        this.COL = width
        this.ROW = height
        this.foundRowsCounter = 0
        this.timeOut = getTimer
        this.timer = this.timeOut
    }
    // help function for creating the Matrix "board" for the gamefield
    createMatrix(w, h) {
        this.width = w
        this.height = h
        const matrix = []
        while (h--) {
            matrix.push(new Array(w).fill(0))
        }
        return matrix
    }
    // popUp
    popUpWindow(player1, player2, form) {
        console.log(player1.playerName + " sercer  " + player2.playerName)
        document.getElementById("spanWinner").innerHTML = player2.playerName
        this.popup(player1, player2)
    }
    timeOutWinner(player1, player2) {
        // check  the wining case
        console.log(player1.scoreOfBoxLeft + "score " + player2.scoreOfBoxLeft)
        console.log(player1.playerName + "name  " + player2.playerName)
        document.getElementById("spanWinner").innerHTML = player1.playerName
        if (player1.scoreOfBoxLeft === player2.scoreOfBoxLeft) {
            document.getElementById("spanWinner").innerHTML = "draw, no winner"
            this.popup(player1, player2)
        } else if (player1.scoreOfBoxLeft > player2.scoreOfBoxLeft) {
            document.getElementById("spanWinner").innerHTML = player1.playerName
            this.popup(player1, player2)
            console.log("_____")
        } else {
            document.getElementById("spanWinner").innerHTML = player2.playerName
            this.popup(player1, player2)
        }
    }
    popup(player1, player2) {
        $('#popup2').popup('show')
        document.getElementById("spanName").innerHTML = player1.playerName
        document.getElementById("spanlines").innerHTML = player1.linesOfBoxLeft
        document.getElementById("spanscores").innerHTML = player1.scoreOfBoxLeft
        // enemy infos
        document.getElementById("spanEnemyName").innerHTML = player2.playerName
        document.getElementById("spanEnemyLines").innerHTML = player2.linesOfBoxLeft
        document.getElementById("spanEnemyScores").innerHTML = player2.scoreOfBoxLeft
    }
    // drawing the boards
    drawBoard(context, board, height, form, assets) {
        this.rowsLength = this.rowsWithOut0(board).length
        for (let i = 0; i < board.length; i++) {
            for (let j = 0; j < board[i].length; j++) {
                if (board[i][j] != 0) {
                    form.generalDraw(context, i, j, height, 0, 0, board, assets, form.spriteSheet[board[i][j]].z, form.spriteSheet[board[i][j]].k)
                }
            }
        }
    }
    // adding lines to the player since a line was removed by his enemy
    addLines(board, leng) {
        for (let i = 0; i < leng; i++) {
            board.splice(0, 1)
            board.push(this.listRandom())
        }
    }
    // random list numbers 0 8
    listRandom() {
        let arr = [8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8]
        let random_0_stellig = Math.floor(Math.random() * this.width) // between 0 and 12(width)
        arr[random_0_stellig] = 0
        return arr
    }
    // check whether the form is up of the board
    gameOverCheck() {
        for (let i = 0; i < 12; i++) {
            if (this.board[1][i] != 0) {
                return true
            }
        }
        return false
    }
    // remove lines if  completed
    removeLines(board) {
        var rowsWithOut0 = this.rowsWithOut0(board)
        // iteration over rows without 0's
        //[17,18,19] 0 1 2
        //3
        while (this.foundRowsCounter != rowsWithOut0.length) {
            for (let y = rowsWithOut0[this.foundRowsCounter]; y >= 1; y--) {
                for (let c = 0; c < this.width; c++) {
                    // sweeping
                    board[y][c] = board[y - 1][c]
                }
            }
            this.foundRowsCounter++
            // if there are no free rows anymore
            for (let c = 0; c < this.width; c++) {
                board[0][c] = 0
            }
            //
        }
        this.foundRowsCounter = 0
    }
    // to find rows without 0's
    rowsWithOut0(items) {
        const noZeroesIndices = []
        for (let i = 0; i < items.length; i++) {
            const row = items[i] //1
            //console.log(items[0])
            let anyZeros = false
            for (let index = 0; index < row.length; index++) {
                if (row[index] === 0) { //1===0?
                    anyZeros = true
                    break
                }
            }
            if (anyZeros === false) {
                this.foundRows++
                //this.checkedLine = true
                noZeroesIndices.push(i)
            }
        }
        return noZeroesIndices
    }
    // how many lines was removed?
    foundLines(arr) {
        let result = 1
        for (let i = 0; i < arr.length; i++) {
            result++
        }
        return result
    }
    // clearing the board after entry the sec. player
    clearBoard(board, form) {
        form.x = 5
        form.y = 0
        for (let i = 0; i < this.height; i++) {
            for (let j = 0; j < this.width; j++) {
                board[i][j] = 0
            }
        }
    }
    // drawing the grid of the boards
    drawgrid(context, form) {
        const h = this.height // here: 20
        const w = this.width // here: 12
        for (let i = 0; i <= w; i++) {
            for (let j = 0; j <= h; j++) {
                this.drawLine(form.convert(i, context, this.height), 0, form.convert(i, context, this.height), context.canvas.height, context) // vertikal
                this.drawLine(0, form.convert(j, context, this.height), context.canvas.width, form.convert(j, context, this.height), context) // horizontal
            }
        }
    }
    // drawing the lines (help function)
    drawLine(x1, y1, x2, y2, context) {
        context.beginPath()
        context.moveTo(x1, y1)
        context.lineTo(x2, y2)
        context.stroke()
    }
    // merg the form with the board
    merge(boardLeft, form) {
        for (let i = 0; i < form.Array2d.length; i++) {
            for (let j = 0; j < form.Array2d.length; j++) {
                if (form.Array2d[i][j] != 0) {
                    boardLeft.board[i + form.y][j + form.x] = form.Array2d[i][j]
                }
            }
        }
    }
}