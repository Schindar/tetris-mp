import Form from './Form.js'
import Board from './Board.js'
import Box from './Box'
import Player from './Player'
import Client from './Client'
import AssetLoader from './AssetLoader'
// Constants for collision
const enumCollision = {
    NOCOL: 0,
    LEFTCOL: 1,
    RIGHTCOL: 2,
    BOTTOMCOL: 3,
    RIGHTFORMCOL: 4,
    LEFTFORMCOL: 5,
    BOTTOMFORMCOL: 6,
    FORMCOL: 7
}
class GameField {
    constructor(boxLeft, boxRight, canvasLeft, canvasRight, height, width, assets) {
        this.assets = assets
        // set the variable of the timer
        this.timerCounter = 0
        this.timerInterval = 1000
        // socket connection
        this.socket = io.connect()
        // sounds
        this.PieceMoveSound = document.getElementById('PieceMove')
        //drop down sound effect
        this.dropDownSoundEffect = document.getElementById('dropDown')
        // sound rotete
        this.rotateSoundEffect = document.getElementById('rotateSound')
        // remove line sound effect
        this.removeLinesSoundEffect = document.getElementById('removeLinesSound')
        // gamve over sound
        this.GameOverSound = document.getElementById('GameOverSound')
        // touch sounds
        this.PieceTouchDownSound = document.getElementById('PieceTouchDown')
        // soft down
        this.softDownSound = document.getElementById('softDown')
        // set instance variables
        this.canvasLeft = canvasLeft
        this.canvasRight = canvasRight
        this.height = height
        this.width = width
        // Boxleft
        this.canvasboxLeft = document.getElementById(boxLeft)
        this.contextboxLeft = this.canvasboxLeft.getContext('2d')
        // BoxRight
        this.canvasboxRight = document.getElementById(boxRight)
        this.contextboxRight = this.canvasboxRight.getContext('2d')
        //players
        this.player1 = new Player(this.form, this.boardLeft, this.boardRight)
        this.player2 = new Player(this.form, this.boardLeft, this.boardRight)
        // Box objects
        this.boxLeft = new Box()
        this.boxRight = new Box()
        // left canvas
        this.canvasLeft = document.getElementById(canvasLeft)
        this.contextCanvaseLeft = this.canvasLeft.getContext('2d')
        // right canvas
        this.canvasRight = document.getElementById(canvasRight)
        this.contextCanvaseRight = this.canvasRight.getContext('2d')
        // new form for board left
        this.form = new Form(5, 0, 'L', this.assets)
        // new form for board right
        this.formRight = new Form(5, 0, 'L', this.assets)
        //new board left
        this.boardLeft = new Board(width, height, this.player1.getTimer, this.form, this.assets)
        // new board right
        this.boardRight = new Board(width, height, this.player2.getTimer, this.formRight, this.assets)
        // set animating variables
        this.Counter = 0
        this.dropInterval = 1000
        this.then = 0
        // listners
        document.addEventListener('keydown', this.player1.move.bind(this))
        document.addEventListener('keyup', this.player1.moveKeyUp.bind(this))
        // new client object
        this.client = new Client(this.boardLeft, this.form, this.player1, this.player2, this.boxLeft, this.boxRight, this.boardRight)
        // excute client functions
        this.client.over(this.socket, this.player1, this.player2, this.form, this.boardRight)
        this.client.timeOutWinnerServer(this.socket, this.player1, this.player2, this.boardLeft)
        this.client.player2IsOnline(this.socket)
        this.client.addLinesServer(this.boardRight, this.boardLeft, this.socket)
        // start animating
        this.startAnimating(0)
    }
    // animating function
    startAnimating(now) {
        const elapsed = now - this.then
        this.client.updateData(this.form, this.boardLeft, this.player1, this.boardRight, this.player2, this.formRight, this.socket)
        this.Counter += elapsed
        if (this.Counter >= this.dropInterval) {
            if (this.form.lock === false && this.formRight.lock === false) {
                this.update()
            }
            this.Counter = 0
        }
        this.timerCounter += elapsed
        if (this.timerCounter >= this.timerInterval) {
            if (this.form.lock === false && this.formRight.lock === false) {
                if (this.boardLeft.timer != -1) {
                    this.boardLeft.timer--
                } else {
                    this.boardLeft.timer = Infinity
                }
                if (this.player1.status) {
                    this.boardLeft.timer = this.boardLeft.timeOut
                    console.log(this.boardLeft.timer + "  " + this.boardRight.timer)
                    if (this.boardRight.timer > this.boardLeft.timer) {
                        this.boardLeft.timer = this.boardRight.timer
                    } else if (this.boardRight.timer < this.boardLeft.timer) {
                        this.boardLeft.timer = this.boardRight.timer
                    }
                    this.boardLeft.clearBoard(this.boardLeft.board, this.form)
                    this.player1.linesOfBoxLeft = 0
                    this.player1.scoreOfBoxLeft = 0
                    this.player1.status = false
                }
                this.boardRight.timer--
            }
            this.timerCounter = 0
        }
        if (this.form.lock === false && this.formRight.lock === false) {
            var currentCollision = this.collisionCheck(this.form.x, this.form.y) // testing the form after movement
            this.collisionHandler(currentCollision) // what to do, if there is a collision
            this.draw()
        }
        if (this.boardLeft.rowsLength != 0) {
            this.boardLeft.removeLines(this.boardLeft.board)
            this.player1.linesOfBoxLeft += this.boardLeft.rowsLength
            this.player1.scoreOfBoxLeft += 10
            this.removeLinesSoundEffect.play()
            if (this.boardLeft.rowsLength === 4) {
                this.player1.scoreOfBoxLeft += 20
            }
            this.socket.emit('added', this.boardLeft.rowsLength)
        }
        this.then = now
        requestAnimationFrame(this.startAnimating.bind(this))
    }
    // Update-function: Moving down
    update() {
        this.form.dropDown() // active form is moving down
    }
    // Collisioncheck
    collisionCheck(x, y) {
        const COL = this.boardLeft.COL // here: 12 Columns
        const ROW = this.boardLeft.ROW // here: 20 Rows
        for (let i = 0; i < this.form.Array2d.length; i++) {
            for (let j = 0; j < this.form.Array2d.length; j++) {
                if (this.form.Array2d[i][j] === 0) {
                    continue // if it´s empty, we skip it
                }
                // coordinates in the gamefield of each square
                let squareX = x + j // x-coordinate +0 or +1 or +2 or ...
                let squareY = y + i // y-coordinate +0 or +1 oder +2 or ...
                // Leftcollision
                if (squareX < 0) {
                    return enumCollision.LEFTCOL
                }
                // Rightcollision
                if (squareX >= COL) {
                    return enumCollision.RIGHTCOL
                }
                // Bottomcollision
                if (squareY >= ROW) {
                    return enumCollision.BOTTOMCOL
                }
                // Formcollision
                if (this.boardLeft.board[squareY][squareX] != 0 && this.form.rotdir == 1) { // collision because of rotation
                    return enumCollision.FORMCOL
                }
                if (this.boardLeft.board[squareY][squareX] != 0 && this.form.xp < this.form.x) { // collision from right
                    return enumCollision.RIGHTFORMCOL
                }
                if (this.boardLeft.board[squareY][squareX] != 0 && this.form.xp > this.form.x) { // collision from left
                    return enumCollision.LEFTFORMCOL
                }
                if (this.boardLeft.board[squareY][squareX] != 0 && this.form.y > this.form.yp && this.form.xp == this.form.x) { // collision from the bottom
                    return enumCollision.BOTTOMFORMCOL
                }
            }
        }
        return enumCollision.NOCOL
    }
    // Collision Handling
    collisionHandler(currentCol) {
        if (this.boardLeft.gameOverCheck() || this.boardLeft.timer === 0) {
            if (this.boardLeft.timer === 0) {
                this.GameOverSound.play()
                this.form.lock = true
                this.boardLeft.timeOutWinner(this.player1, this.player2)
                this.socket.emit('timeOutWinner', this.player1)
            } else {
                this.GameOverSound.play()
                this.form.lock = true
                this.boardLeft.popUpWindow(this.player1, this.player2, this.form)
                this.socket.emit('over', this.player2)
            }
        }
        switch (currentCol) {
            // Leftcollision handling
            case enumCollision.LEFTCOL:
                this.form.x += 1
                break
                // Rightcollision handling
            case enumCollision.RIGHTCOL:
                this.form.x -= 1
                break
                // Bottomcollision handling
            case enumCollision.BOTTOMCOL:
                this.form.y -= 1
                this.boardLeft.merge(this.boardLeft, this.form)
                var x = this.form.typenext
                this.form = new Form(5, 0, x)
                this.dropInterval = 1000
                this.PieceTouchDownSound.play()
                break
                // Formcollision handling
            case enumCollision.LEFTFORMCOL:
                this.form.x += 1
                break
            case enumCollision.RIGHTFORMCOL:
                this.form.x -= 1
                break
            case enumCollision.FORMCOL:
                this.form.rotatereverse(this.form.Array2d)
                break
            case enumCollision.BOTTOMFORMCOL:
                this.form.y -= 1
                this.boardLeft.merge(this.boardLeft, this.form)
                var x = this.form.typenext
                this.form = new Form(5, 0, x)
                this.dropInterval = 1000
                this.PieceTouchDownSound.play()
                break
                // No Collision :) enumCollision.NOCOL
            default:
                // Do nothing
        }
    }
    // drop down sounds
    // draw
    draw() {
        // clear canvases
        this.contextCanvaseRight.clearRect(0, 0, this.contextCanvaseRight.canvas.width, this.contextCanvaseRight.canvas.height)
        this.contextCanvaseLeft.clearRect(0, 0, this.contextCanvaseLeft.canvas.width, this.contextCanvaseLeft.canvas.height)
        // clrear canvas Box
        this.contextboxLeft.clearRect(0, 0, this.contextboxLeft.canvas.width, this.contextboxLeft.canvas.height)
        this.contextboxRight.clearRect(0, 0, this.contextboxRight.canvas.width, this.contextboxRight.canvas.height)
        // drawing box left
        let x = this.form.typenext
        this.boxLeft.drawBox("🎮:", 4, 160, this.player1.playerName, this.contextboxLeft)
        this.boxLeft.drawBox("Lines: ", 4, 180, this.player1.linesOfBoxLeft, this.contextboxLeft)
        this.boxLeft.drawBox("Score: ", 4, 200, this.player1.scoreOfBoxLeft, this.contextboxLeft)
        this.boxLeft.drawBox("Next: ", 4, 220, x, this.contextboxLeft) //next stone
        this.boxLeft.drawBox("Timer:", 4, 240, this.boardLeft.timer, this.contextboxLeft)
        // draw next form on the left box canvases
        this.form.drawForm(0, 0, this.form.Array2dnext, this.contextboxLeft, this.height, this.assets)
        // drawing box right
        this.boxRight.drawBox("🎮:", 4, 14, this.player2.playerName, this.contextboxRight)
        this.boxRight.drawBox("Score: ", 4, 45, this.player2.scoreOfBoxLeft, this.contextboxRight)
        this.boxRight.drawBox("Lines: ", 4, 30, this.player2.linesOfBoxLeft, this.contextboxRight)
        this.boxRight.drawBox("Timer:", 4, 65, this.boardLeft.timer, this.contextboxRight)
        // drwing canvases
        this.boardLeft.drawgrid(this.contextCanvaseRight, this.form)
        this.boardLeft.drawgrid(this.contextCanvaseLeft, this.form)
        this.form.drawForm(this.form.x, this.form.y, this.form.Array2d, this.contextCanvaseLeft, this.height, this.assets)
        this.boardLeft.drawBoard(this.contextCanvaseLeft, this.boardLeft.board, this.height, this.form, this.assets)
        this.formRight.drawForm(this.formRight.x, this.formRight.y, this.formRight.Array2d, this.contextCanvaseRight, this.height, this.assets)
        this.boardRight.drawBoard(this.contextCanvaseRight, this.boardRight.board, this.height, this.formRight, this.assets)
    }
}
new AssetLoader()
    .loadAssets([{
        name: 'smileys',
        url: 'css/Fotos/smileys.png'
    }, ])
    .then(assets => {
        new GameField('boxLeft', 'boxRight', 'canvasLeft', 'canvasRight', 20, 12, assets)
    })