export default class Form {
    constructor(x, y, type, assets) {
        this.assets = assets
        this.lock = false
        this.randomNumb
        // x and y position of the form
        this.x = x
        this.xp = x
        this.y = y
        this.yp = y
        this.rotdir = 0
        // Type of form: 'I', 'L', 'J', 'O', 'Z', 'S', 'T'
        this.type = type
        this.typenext = this.createRandomType()
        this.Array2d = this.createForm(this.type)
        this.Array2dnext = this.createForm(this.typenext)
        this.spriteSheet = {
            1: {
                x: 0 * 32,
                y: 0 * 32,
                z: 1 * 32,
                k: 0 * 32,
            },
            2: {
                x: 0 * 32,
                y: 1 * 32,
                z: 1 * 32,
                k: 1 * 32,
            },
            3: {
                x: 0 * 32,
                y: 2 * 32,
                z: 1 * 32,
                k: 2 * 32,
            },
            4: {
                x: 0 * 32,
                y: 3 * 32,
                z: 1 * 32,
                k: 3 * 32,
            },
            5: {
                x: 0 * 32,
                y: 4 * 32,
                z: 1 * 32,
                k: 4 * 32,
            },
            6: {
                x: 0 * 32,
                y: 5 * 32,
                z: 1 * 32,
                k: 5 * 32,
            },
            7: {
                x: 0 * 32,
                y: 6 * 32,
                z: 1 * 32,
                k: 6 * 32,
            },
            8: {
                x: 0 * 32,
                y: 7 * 32,
                z: 1 * 32,
                k: 7 * 32,
            }
        }
    }
    // Creating the different types of forms: 1 means the rectangle should be colored and 0 for !color
    createForm(type) {
        if (type === 'I') {
            return [
                [0, 1, 0, 0],
                [0, 1, 0, 0],
                [0, 1, 0, 0],
                [0, 1, 0, 0],
            ]
        } else if (type === 'L') {
            return [
                [0, 2, 0],
                [0, 2, 0],
                [0, 2, 2],
            ]
        } else if (type === 'J') {
            return [
                [0, 3, 0],
                [0, 3, 0],
                [3, 3, 0],
            ]
        } else if (type === 'O') {
            return [
                [4, 4],
                [4, 4],
            ]
        } else if (type === 'Z') {
            return [
                [5, 5, 0],
                [0, 5, 5],
                [0, 0, 0],
            ]
        } else if (type === 'S') {
            return [
                [0, 6, 6],
                [6, 6, 0],
                [0, 0, 0],
            ]
        } else if (type === 'T') {
            return [
                [0, 7, 0],
                [7, 7, 7],
                [0, 0, 0],
            ]
        }
    }
    // help-function: Creates random Number for random type
    createRandomType() {
        let types = ['I', 'L', 'J', 'O', 'Z', 'S', 'T']
        this.randomNumb = Math.floor(Math.random() * 7)
        return types[this.randomNumb]
    }
    // Moving down one square per tick
    dropDown() {
        if (this.lock === false) {
            this.rotdir = 0
            this.xp = this.x
            this.yp = this.y
            this.y += 1
        }
    }
    // drawing the form
    drawForm(x, y, array, context, height, assets) {
        for (let i = 0; i < array.length; i++) {
            for (let j = 0; j < array.length; j++) {
                if (array[i][j] != 0) {
                    this.generalDraw(context, i, j, height, x, y, array, assets, this.spriteSheet[array[i][j]].x, this.spriteSheet[array[i][j]].y)
                }
            }
        }
    }
    generalDraw(context, i, j, height, x, y, array, assets, spriteSheetX, spriteSheetY) {
        const h = (context.canvas.height / height) - 1.5
        const b = (context.canvas.height / height) - 1.5
        var convertX = this.convert(x + j, context, height)
        var convertY = this.convert(y + i, context, height)
        //spriteSheet[array[i][j]].y
        context.drawImage(
            assets.smileys,
            spriteSheetX,
            spriteSheetY,
            32,
            32,
            convertX,
            convertY,
            h,
            b
        )
    }
    convert(wert, context, height) {
        return wert * context.canvas.height / height
    }
    // Rotating the form
    rotate(matrix) {
        // reverse the rows
        matrix = matrix.reverse()
        // swap the symmetric elements
        for (var i = 0; i < matrix.length; i++) {
            for (var j = 0; j < i; j++) {
                var temp = matrix[i][j]
                matrix[i][j] = matrix[j][i]
                matrix[j][i] = temp
            }
        }
    }
    rotatereverse(matrix) {
        // swap the symmetric elements
        for (var i = 0; i < matrix.length; i++) {
            for (var j = 0; j < i; j++) {
                var temp = matrix[i][j]
                matrix[i][j] = matrix[j][i]
                matrix[j][i] = temp
            }
        }
        // reverse the rows
        matrix = matrix.reverse()
    }
}