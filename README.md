# Multiplayer Puyo Puyo Tetris.




## to start the game

1) go to the folder direction type in in the terminal: cd tetris
2) type in "npm run build && node server.js" in the terminal to run the server


    *  If Error: Der Befehl "npm" ist entweder falsch geschrieben oder konnte nicht gefunden werden
        1. you have to install npm: Therefor install nodejs here: https://nodejs.org/en/download/
           npm is part of the nodejs-installation

    * If Error: Der Befehl "nodemon" ist entweder falsch geschrieben oder konnte nicht gefunden werden
        1. type in "npm intstall -g nodemon" in the terminal

    * If error with webpack please visite this webpage to install the dependancy of webpack: https://webpack.js.org/guides/installation/.


3) for local open localhost in your browser with (http://localhost:8080)

4) for another computer, type in http://YOUR_IP_HIER:8080 in the other computer

## Instructions

* Press key A to go to the LEFT with the active Tetromino

* Press key D to go to the RIGHT with the active Tetromino

* Press key E to ROTATE the active Tetromino

* Press key Q to ROTATE REVERSE the active Tetromino

* Press key D to DROP DOWN once

* Press tab key to go DOWN QUICKLY with the active Tetromino

* Have fun with multiplayer-tetris-game :-)

## License
[Schindar Ali](https://www.linkedin.com/in/schindar/) 
## Photo of the game
![tetris](/uploads/07997942bae0eadaf350a4786b01a464/tetris.png)